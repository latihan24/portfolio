
@extends('layouts.vaseline')

@section('title', 'Details')
@section('content')
<br><br><br>
<div class="mt-5"></div>
<div class="jumbotron jumbotron-fluid">
    <div class="col-8 container">
        <img src="{{ $welan->cover !=null ? asset('storage/cover/'.$welan->cover) : asset('image/nosatu.jpg') }}"
        class="card-img-top" draggable="false" width="500" height="350">
    </div>
  </div>
  <br>
  <div class="text-center">
    <a href="{{$welan->url}}" class="btn btn-outline-danger">Try Me</a>
  </div>
  


<div class="px-5 mt-5">
  <h2>Details</h2>
<br>
<h4>{{$welan->name}}</h4>
<h6>Framework:</h6>
<p class="card-text">
  @foreach ($welan->frameworks as $framework)
          {{ $framework->name }} <br>
      @endforeach
</p>
<h6>keterangan:</h6>
<p>{!! html_entity_decode ($welan->details)!!}</p>

</div>


@endsection
    