@extends('layouts.vaseline')

@section('title', 'Delete Welan')
@section('content')
<br><br><br><br><br>
<h2 class="px-5">Are you sure to delete website / Landing Page {{ $welan->name }} ?</h2>
<div class="px-5 mt-3 mb-5">
    <a href="/welan-destroy/{{ $welan->slug }}" class="btn btn-danger me-3">Sure</a>
    <a href="/welan" class="btn btn-primary">Cancel</a>
</div>

@endsection