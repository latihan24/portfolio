@extends('layouts.vaseline')

@section('title', 'Add WeLan')

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.tiny.cloud/1/m8htpeon53hnriqvgcdre48ftstyxfrk67e0mdf4y7tl4kn9/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

<br><br><br><br><br><br>
<h1 class="px-5">Add Website dan Landing Page </h1>

<div class="px-5 mb-5 mt-5 w-50">

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="welan-add" method="post" enctype="">
        @csrf

        <div class="mb-3">
            <label for="type" class="form-label">type</label>
            <input type="text" name="type" id="code" class="form-control" placeholder="type" value="{{ old('type') }}">
        </div>
        
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" name="name" id="code" class="form-control" placeholder="name" value="{{ old('name') }}">
        </div>

        <div class="mb-3">
            <label for="url" class="form-label">Url</label>
            <input type="text" name="url" id="url" class="form-control" placeholder="url"
            value="{{ old('url') }}">
        </div>

        <div class="mb-3">
            <label for="image" class="form-label">Image</label>
            <input type="file" name="image" class="form-control" >
        </div>

        <div class="mb-3">
            <label for="framework" class="form-label">Framework</label>
            <select name="frameworks[]" id="framework" class="form-control select-multiple" multiple >
                @foreach ($frameworks as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <label for="details" class="form-label">Details</label>
            <textarea id="details" class="form-control  @error('details') is-invalid @enderror" placeholder="details" name="details" value="{{ old('details') }}" required autocomplete="details" autofocus></textarea>

        </div>

        <div class="mt-3 ">
            <button class="btn btn-success" type="submit">Save</button>
        </div>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.6.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
<script>
    $(document).ready(function() {
    $('.select-multiple').select2();
});
</script>


<!--tinymce js-->
<script src="{{ asset('backend/assets/ak/tinymce.min.js') }} "></script>

<!-- init js -->
<script src="{{ asset('backend/assets/ak/form-editor.init.js') }} "></script>


<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'ai tinycomments mentions anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed permanentpen footnotes advtemplate advtable advcode editimage tableofcontents mergetags powerpaste tinymcespellchecker autocorrect a11ychecker typography inlinecss',
    toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | align lineheight | tinycomments | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
    mergetags_list: [
      { value: 'First.Name', title: 'First Name' },
      { value: 'Email', title: 'Email' },
    ],
    ai_request: (request, respondWith) => respondWith.string(() => Promise.reject("See docs to implement AI Assistant"))
  });
</script>



@endsection