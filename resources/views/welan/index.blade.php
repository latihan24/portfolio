@extends('layouts.vaseline')

@section('title', 'WeLan')
@section('content')
<br><br><br><br><br><br>
<h1 class="px-5">Website dan Landing Page List</h1>
    <div class="mt-5 d-flex justify-content-end">
        <a href="/welan-deleted" class="btn btn-secondary me-3">View Deleted Data</a>
        <a href="/welan-add" class="btn btn-primary">add data</a>
    </div>

    <div clas="mt-5">
        @if(session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
    </div>

    <div class="px-5 mb-5 my-5">
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>URL</th>
                    <th>Framework</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($welans as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->type }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->url }}</td>
                        <td>
                            @foreach ($item->frameworks as $framework)
                                {{ $framework->name }} <br>
                            @endforeach
                        </td>
                        <td><a href="/welan-edit/{{ $item->slug }}">edit</a>
                            <a href="/welan-delete/{{ $item->slug }}">delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection