@extends('layouts.vaseline')

@section('title', 'Deleted Website')
@section('content')
<br><br><br><br><br><br>
<h1 class="px-5">Deleted Website List</h1>

<div clas="mt-5">
    @if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
</div>

<div class="px-5 my-5 ">
    <table class="table">
        <thead>
            <tr>
                <th>No.</th>
                <th>Type</th>
                <th>Name</th>
                <th>URL</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($deletedwelans as $item)
            <tr>
                <td>{{ $loop -> iteration }}</td>
                <td>{{ $item->type }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->url }}</td>
                <td>
                    <a href="welan-restore/{{ $item->slug }}">restore</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection