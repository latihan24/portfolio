<header class="header_area sticky-header">
    <div class="main_menu">
        <nav class="navbar navbar-expand-lg navbar-light main_box">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <a class="navbar-brand logo_h" href="/"><img src="{{asset('stella/img/logo.png')}}" width="160" height="60" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                    <ul class="nav navbar-nav menu_nav ml-auto">
                        <li class="nav-item {{ request()->is('/') ? 'active' : '' }}"><a class="nav-link" href="/">Home</a></li>
                        @if (Auth::user())
                        @if (Auth::user()->role_id == 1)
                        <li class="nav-item {{ request()->is('website') ? 'active' : '' }}"><a class="nav-link" href="/website" >Gallery</a></li>
                        <li class="nav-item {{ request()->is('welan') ? 'active' : '' }}"><a class="nav-link" href="/welan"
                                @if(
                                request()->route()->uri=='welan-add'||
                                request()->route()->uri=='welan-deleted'||
                                request()->route()->uri=='welan-edit/{slug}'||
                                request()->route()->uri=='welan-delete/{slug}') class='active' 
                                @endif>Tambah Website</a>
                        </li>
                        <li class="nav-item {{ request()->is('framework') ? 'active' : '' }}"><a class="nav-link" href="/framework"
                                @if(
                                request()->route()->uri=='framework-add'||
                                request()->route()->uri=='framework-deleted'||
                                request()->route()->uri=='framework-deleted'||
                                request()->route()->uri=='framework-edit/{slug}'||
                                request()->route()->uri=='framework-delete/{slug}') class='active' @endif>Frameworks</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="text-white btn btn-secondary"
                                href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>

                        @else
                        <li class="nav-item {{ request()->is('website') ? 'active' : '' }}"><a class="nav-link" href="/website">Gallery</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>

                        @endif
                        @else
                        <li class="nav-item {{ request()->is('login') ? 'active' : '' }}"><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                        <li class="nav-item {{ request()->is('register') ? 'active' : '' }}"><a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a></li>
                        @endif
                        
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>