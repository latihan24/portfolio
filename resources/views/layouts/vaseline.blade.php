<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Ahsani - Home</title>
	<link rel="icon" href="{{asset('stella/img/Fevicon.png')}}" type="image/png">

  <link rel="stylesheet" href="{{asset('stella/vendors/bootstrap/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('stella/vendors/fontawesome/css/all.min.css')}}">
  <link rel="stylesheet" href="{{asset('stella/vendors/themify-icons/themify-icons.css')}}">
  <link rel="stylesheet" href="{{asset('stella/vendors/flat-icon/flaticon.css')}}">
	<link rel="stylesheet" href="{{asset('stella/vendors/nice-select/nice-select.css')}}">
  <link rel="stylesheet" href="{{asset('stella/vendors/Magnific-Popup/magnific-popup.css')}}">	
  <link rel="stylesheet" href="{{asset('stella/vendors/OwlCarousel/owl.theme.default.min.css')}}">
  <link rel="stylesheet" href="{{asset('stella/vendors/OwlCarousel/owl.carousel.min.css')}}">

  <link rel="stylesheet" href="{{asset('stella/css/style.css')}}">

 
  
</head>
<body>
  <!-- ================ start header Area ================= -->  
	@include('partial.nav')
  <!-- ================ end header Area ================= -->  
  
       
@yield('content')

  <!-- ================ start footer Area ================= -->
  <footer class="footer-area section-gap">
		<div class="container">
			<div class="footer-bottom row align-items-center text-center ">
				<p class="footer-text m-0 col-md-12"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="#" target="_blank">Ahsani</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				
			</div>
		</div>
	</footer>
  <!-- ================ End footer Area ================= -->

  <script src="{{asset('stella/vendors/jquery/jquery-3.2.1.min.js')}}"></script>
  <script src="{{asset('stella/vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('stella/vendors/OwlCarousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('stella/vendors/sticky/jquery.sticky.js')}}"></script>
  <script src="{{asset('stella/js/jquery.ajaxchimp.min.js')}}"></script>
	<script src="{{asset('stella/js/mail-script.js')}}"></script>
  <script src="{{asset('stella/vendors/Magnific-Popup/jquery.magnific-popup.min.js')}}"></script>	
  <script src="{{asset('stella/js/main.js')}}"></script>
  
  <!--tinymce js-->
  <script src="{{ asset('backend/assets/ak/tinymce.min.js') }} "></script>

  <!-- init js -->
  <script src="{{ asset('backend/assets/ak/form-editor.init.js') }} "></script>


  <script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'ai tinycomments mentions anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount checklist mediaembed casechange export formatpainter pageembed permanentpen footnotes advtemplate advtable advcode editimage tableofcontents mergetags powerpaste tinymcespellchecker autocorrect a11ychecker typography inlinecss',
      toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table mergetags | align lineheight | tinycomments | checklist numlist bullist indent outdent | emoticons charmap | removeformat',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      mergetags_list: [
        { value: 'First.Name', title: 'First Name' },
        { value: 'Email', title: 'Email' },
      ],
      ai_request: (request, respondWith) => respondWith.string(() => Promise.reject("See docs to implement AI Assistant"))
    });
  </script>

</body>
</html>