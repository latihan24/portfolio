<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rental Buku | @yield('title')</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
    <link rel="stylesheet" href="{{asset ('css/style.css') }}">

    <link rel="icon" href="{{asset('stella/img/Fevicon.png')}}" type="image/png">

  <link rel="stylesheet" href="{{asset('stella/vendors/bootstrap/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('stella/vendors/fontawesome/css/all.min.css')}}">
  <link rel="stylesheet" href="{{asset('stella/vendors/themify-icons/themify-icons.css')}}">
  <link rel="stylesheet" href="{{asset('stella/vendors/flat-icon/flaticon.css')}}">
	<link rel="stylesheet" href="{{asset('stella/vendors/nice-select/nice-select.css')}}">
  <link rel="stylesheet" href="{{asset('stella/vendors/Magnific-Popup/magnific-popup.css')}}">	
  <link rel="stylesheet" href="{{asset('stella/vendors/OwlCarousel/owl.theme.default.min.css')}}">
  <link rel="stylesheet" href="{{asset('stella/vendors/OwlCarousel/owl.carousel.min.css')}}">

  <link rel="stylesheet" href="{{asset('stella/css/style.css')}}">
</head>


<body class="bodyas">
    <div class="main d-flex flex-column justify-content-between">
        <nav class="navbar  navbar-expand-lg navtas">
            <div class="container-fluid">

                <a class="navbar-brand navtas" href="/">Rental Buku</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

            </div>

        </nav>

        <div class="body-content h-100 ">

            <div class="row g-0 h-100">
                <div class="sidebar col-lg-2 collapse d-lg-block" id="navbarTogglerDemo03">

                    @if (Auth::user())
                    @if (Auth::user()->role_id == 1)
                    {{--<li><a href="dashboard">Dashboard</a></li>
                        <li>welan-</li>
                        <li>Categories</li>
                        <li>Users</li>
                        <li>Rent Log</li>
                        <li>Logout</li>--}}
                    <a href="/website" @if(request()->route()->uri=='website') class='active' @endif>Website</a>
                    <a href="/landingpage" @if(request()->route()->uri=='landingpage') class='active' @endif>Landing
                        Page</a>
                    <a href="/welan" @if(request()->route()->uri=='welan'|| request()->route()->uri=='welan-add'||
                        request()->route()->uri=='welan-deleted'|| request()->route()->uri=='welan-edit/{slug}'||
                        request()->route()->uri=='welan-delete/{slug}') class='active' @endif>WeLan</a>
                    <a href="/framework" @if(request()->route()->uri=='framework'||
                        request()->route()->uri=='framework-add'|| request()->route()->uri=='framework-deleted'||
                        request()->route()->uri=='framework-deleted'||
                        request()->route()->uri=='framework-edit/{slug}'||
                        request()->route()->uri=='framework-delete/{slug}') class='active' @endif>Frameworks</a>

                    <a class="text-white btn btn-secondary" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                    @else
                    {{--<li>Profile</li>
                        <li>Logout</li>--}}
                    <a href="/website" @if(request()->route()->uri=='website') class='active' @endif>Website</a>
                    <a href="/landingpage" @if(request()->route()->uri=='landingpage') class='active' @endif>Landing
                        Page</a>
                    <a class="text-white btn btn-secondary" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>

                    @endif
                    @else

                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>


                    @endif



                </div>
                <div class="content p-5 col-lg-10">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <div>

    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous">
    </script>


<script src="https://cdn.ckeditor.com/ckeditor5/35.4.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#details' ) )
        .catch( error => {
            console.error( error );
        } );
</script>


<script src="{{asset('stella/vendors/jquery/jquery-3.2.1.min.js')}}"></script>
  <script src="{{asset('stella/vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('stella/vendors/OwlCarousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('stella/vendors/sticky/jquery.sticky.js')}}"></script>
  <script src="{{asset('stella/js/jquery.ajaxchimp.min.js')}}"></script>
	<script src="{{asset('stella/js/mail-script.js')}}"></script>
  <script src="{{asset('stella/vendors/Magnific-Popup/jquery.magnific-popup.min.js')}}"></script>	
  <script src="{{asset('stella/js/main.js')}}"></script>

</body>

</html>