@extends('layouts.vaseline')

@section('title', 'Website')
@section('content')

<main class="site-main">

    <!-- ================ start hero banner Area ================= -->
    <section class="hero-banner">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="hero-banner__img">
                        <img class="img-fluid" src="{{asset('stella/img/banner/gucci.jpg')}}" width="572" height="9550"alt="">
                    </div>
                </div>
                <div class="col-md-6 pl-xl-4">
                    <div class="hero-banner__content">
                        <h1>Hi, i'm Dwina Ahsani <span>Looking to build a website ?</span></h1>
                        <p>Are you looking to build a new startup website or make your current site more effective? How much does it cost to build a website?
                           <br> you can contact me, klik my link WhatsApp
                        </p>
                        
                        <a href="https://api.whatsapp.com/send?phone=6285281547033"><button class="button button-hero">WhatsApp</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ================ end hero banner Area ================= -->

    <!-- ================ start feature section ================= -->
    <section class="section-margin--large">
        <div class="container">
            <div class="section-intro pb-70px">
                <a href="/website">
                    <h4 class="section-intro__title">Click to view more</h4>
                </a>
                <h2 class="section-intro__subtitle">Here's My Portfolio</h2>
            </div>

            <div class="row gutters-48">

                @foreach ($welan as $item)
    
                <div class="col-md-6 col-xl-3 mb-5 mt-5 mb-xl-0">
                    <br><br>
                    <div class="card text-center card-feature border-style h-100">
                        <a href="/welan/{{ $item->slug }}">
                            <img src="{{ $item->cover !=null ? asset('storage/cover/'.$item->cover) : asset('image/nosatu.jpg') }}"
                                class="card-img-top" draggable="false" width="255" height="150">
                            <div class="card-body">
                                <h5 class="card-feature__title">{{ $item->name }}</h5>
                                <p class="card-text">
                                    @foreach ($item->frameworks as $framework)
                                    {{ $framework->name }} <br>
                                    @endforeach
                                </p>
                        </a>
                    </div>
                </div>
    
            </div>
            @endforeach
            
        </div>
    </section>
    <!-- ================ end feature section ================= -->






</main>
@endsection