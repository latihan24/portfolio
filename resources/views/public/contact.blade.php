@extends('layouts.vaseline')

@section('title', 'Contact')
@section('content')

<!-- ================ start banner area ================= -->
<section class="banner-area about" id="about">
    <div class="container h-100">
        <div class="banner-area__content text-center">
            <h1>Contact</h1>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contact</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<!-- ================ end banner area ================= -->

<!-- ================ start feature section ================= -->


@endsection