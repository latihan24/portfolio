@extends('layouts.vaseline')

@section('title', 'About')
@section('content')

<!-- ================ start banner area ================= -->
<section class="banner-area about" id="about">
    <div class="container h-100">
        <div class="banner-area__content text-center">
            <h1>About</h1>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">About</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<!-- ================ end banner area ================= -->



@endsection