@extends('layouts.vaseline')

@section('title', 'Gallery')
@section('content')

<!-- ================ start banner area ================= -->
<section class="banner-area about" id="about">
    <div class="container h-100">
        <div class="banner-area__content text-center">
            <h1>Gallery</h1>
            <nav aria-label="breadcrumb" class="banner-breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Gallery</li>
                </ol>
            </nav>
        </div>
    </div>
</section>
<!-- ================ end banner area ================= -->

<!-- ================ start feature section ================= -->
<section class="section-margin--large pt-xl-5">
    <div class="container">
        <div class="section-intro pb-70px">
            <h4 class="section-intro__title">My Art</h4>
            <h2 class="section-intro__subtitle">Here's My Portfolio</h2>
        </div>

        <div class="row gutters-48 ">

            @foreach ($welan as $item)

            <div class="col-md-6 col-xl-3 mb-5 mt-5 mb-xl-0 ">
                <br><br>
                <div class="card text-center card-feature border-style h-100">
                    <a href="/welan/{{ $item->slug }}">
                        <img src="{{ $item->cover !=null ? asset('storage/cover/'.$item->cover) : asset('image/nosatu.jpg') }}"
                            class="card-img-top" draggable="false" width="255" height="150">
                        <div class="card-body">
                            <h5 class="card-feature__title">{{ $item->name }}</h5>
                            <p class="card-text">
                                @foreach ($item->frameworks as $framework)
                                {{ $framework->name }} <br>
                                @endforeach
                            </p>
                    </a>
                </div>
            </div>

        </div>
        @endforeach



    </div>
    </div>
</section>
<!-- ================ end feature section ================= -->

@endsection