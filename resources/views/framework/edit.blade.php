@extends('layouts.vaseline')

@section('title', 'Edit Framework')
@section('content')
<br><br><br><br><br>
<h1 class="px-5">Edit Framework</h1>
<div class="px-5 mb-5 mt-5 w-50">

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="/framework-edit/{{ $framework->slug }}" method="post">
        @csrf
        @method('put')
        <div>
            <label for="name" class="form-label">Name</label>
            <input type="text" name="name" id="name" class="form-control" value="{{ $framework->name }}" >
        </div>

        <div class="mt-3 ">
            <button class="btn btn-success" type="submit">Update</button>
        </div>
    </form>
</div>

@endsection