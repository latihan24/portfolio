<?php

namespace App\Http\Controllers;

use App\Models\collab_wlf;
use Illuminate\Http\Request;

class CollabWlfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\collab_wlf  $collab_wlf
     * @return \Illuminate\Http\Response
     */
    public function show(collab_wlf $collab_wlf)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\collab_wlf  $collab_wlf
     * @return \Illuminate\Http\Response
     */
    public function edit(collab_wlf $collab_wlf)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\collab_wlf  $collab_wlf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, collab_wlf $collab_wlf)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\collab_wlf  $collab_wlf
     * @return \Illuminate\Http\Response
     */
    public function destroy(collab_wlf $collab_wlf)
    {
        //
    }
}
