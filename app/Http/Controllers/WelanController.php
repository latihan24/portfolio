<?php

namespace App\Http\Controllers;

use App\Models\Framework;
use App\Models\Welan;
use Illuminate\Http\Request;

class WelanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $welans = Welan::all();
        return view('welan.index',['welans' => $welans]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $frameworks = Framework::all();
        return view('welan.create',['frameworks' => $frameworks ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate =$request->validate([
            'type' => 'required',
            'name' => 'required',
            'url' => 'required',
            'details' => 'required',
        ]);

        $newName = '';
        if($request->file('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $newName = $request->title.'-'.now()->timestamp.'.'.$extension;
            $request->file('image')->storeAs('cover', $newName);
        }
        

        $request['cover'] = $newName;
        $welan =Welan::create($request->all());
        $welan->frameworks()->sync($request->frameworks);
        return redirect('welan')->with('status', 'Welan added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\welan  $welan
     * @return \Illuminate\Http\Response
     */
    public function show(welan $welan,$slug)
    {
        $framework = Framework::where ('slug', $slug)->first();
        $welan = Welan::where ('slug', $slug)->first();
        return view('welan.show',['welan' => $welan,'framework' => $framework]);
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\welan  $welan
     * @return \Illuminate\Http\Response
     */
    public function edit(welan $welan,$slug)
    {
        $welan = Welan::where ('slug', $slug)->first();
        $frameworks = Framework::all();
        return view('welan.edit',['welan' => $welan,'frameworks' => $frameworks]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\welan  $welan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, welan $welan,$slug)
    {
        if($request->file('image')){
            $extension = $request->file('image')->getClientOriginalExtension();
            $newName = $request->title.'-'.now()->timestamp.'.'.$extension;
            $request->file('image')->storeAs('cover', $newName);
            $request['cover'] = $newName;
        }
        
        
        
        $welan = Welan::where('slug',$slug)->first();
        $welan->update($request->all());

        if($request->frameworks){
            $welan->frameworks()->sync($request->frameworks);
        }
        return redirect('welan')->with('status', 'welan updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\welan  $welan
     * @return \Illuminate\Http\Response
     */
    
    public function delete($slug)
    {
        $welan = Welan::where('slug', $slug)->first();
        return view('welan.delete',['welan' => $welan]);
    }

    public function destroy(Welan $welan, $slug)
    {
        $welan = Welan::where('slug', $slug)->first();
        $welan->delete();
        return redirect('welan')->with('status', 'welan deleted Successfully');
    }
    public function deletedwelan()
    {
        $deletedwelans = Welan::onlyTrashed()->get();
        return view('welan.deleted-list',['deletedwelans' => $deletedwelans]);
    }
    public function restore($slug)
    {
        $welan = Welan::withTrashed()->where('slug', $slug)->first();
        $welan->restore();
        return redirect('welan')->with('status', 'welan Restored Successfully');
    }
}
