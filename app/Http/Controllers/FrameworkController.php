<?php

namespace App\Http\Controllers;

use App\Models\Framework;
use Illuminate\Http\Request;

class FrameworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frameworks=Framework::all();
        return view ('framework.index',['frameworks'=>$frameworks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('framework.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:frameworks|max:255',
        ]);
        $framework = Framework::create($request->all());
        return redirect('framework')->with('status', 'Framework added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\framework  $framework
     * @return \Illuminate\Http\Response
     */
    public function show(framework $framework)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\framework  $framework
     * @return \Illuminate\Http\Response
     */
    public function edit(Framework $framework,$slug)
    {
        $framework = Framework::where('slug', $slug)->first();
        return view('framework.edit',['framework' => $framework]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\framework  $framework
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, framework $framework, $slug)
    {
        $validated = $request->validate([
            'name' => 'required|unique:frameworks|max:255',
        ]);
        $framework = Framework::where('slug', $slug)->first();
        $framework->slug =null;
        $framework ->update($request->all());
        return redirect('framework')->with('status', 'framework updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\framework  $framework
     * @return \Illuminate\Http\Response
     */
    public function destroy(framework $framework, $slug)
    {
        $framework = Framework::where('slug',$slug)->first();
        $framework ->delete();
        return redirect('framework')->with('status', 'framework deleted');
    }
}
