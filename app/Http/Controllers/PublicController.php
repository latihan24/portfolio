<?php

namespace App\Http\Controllers;

use App\Models\Framework;
use App\Models\Welan;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function wbs()
    {
        
        $welan=Welan::where('type','website')->get();
        return view('public.website',['welan'=>$welan]);
    }

    public function lp()
    {
        
        $welan=Welan::where('type','landingpage')->get();
        return view('public.landingpage',['welan'=>$welan]);
    }

    public function lpdashboard()
    {
        
        $welan=Welan::where('type','lp.dashboard')->get();
        return view('public.lpdashboard',['welan'=>$welan]);
    }

    public function about()
    {
        return view('public.about');
    }

    public function contact()
    {
        return view('public.contact');
    }

    
    
}
