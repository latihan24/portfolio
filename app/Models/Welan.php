<?php

namespace App\Models;


use App\Models\Type;
use App\Models\Framework;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Welan extends Model
{
    use HasFactory;
    use Sluggable;
    use SoftDeletes;

    protected $guarded = ['id'];
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
  
    public function frameworks(): BelongsToMany
    {
        return $this ->belongsToMany(Framework::class, 'collab_wlves', 'welan_id', 'framework_id');
    }
    public function types()
    {
        return $this->belongsTo(Type::class);
    }
}
