<?php

namespace Database\Seeders;

use App\Models\Framework;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class FrameworkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Framework::truncate();
        Schema::enableForeignKeyConstraints();
        
        $data= [
            'Laravel','Bootstrap ','React.Js'
        ];

        foreach ($data as $value) {
            Framework::insert([
                'name' => $value,
                'slug' => $value,
            ]);
        }
    }
}
