<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collab_wlves', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('welan_id');
            $table->foreign('welan_id')->references('id')->on('welans');
            $table->unsignedBigInteger('framework_id');
            $table->foreign('framework_id')->references('id')->on('frameworks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collab_wlves');
    }
};
