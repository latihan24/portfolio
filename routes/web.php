<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WelanController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\FrameworkController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/test', function () {
   // return view('welcome');
//})

//route::get('/va',function () {
  //  return view('vaseline');
//});


Route::get('/', [PublicController::class, 'lpdashboard']);
//Route::get('/about', [PublicController::class, 'about']);
//Route::get('/contact', [PublicController::class, 'contact']);

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');




Route::middleware('auth')->group(function(){

    Route::get('website', [PublicController::class, 'wbs']);
    //Route::get('landingpage', [PublicController::class, 'lp']);
    Route::get('welan/{slug}', [WelanController::class, 'show']);

    Route::middleware('only_admin')->group(function(){

        Route::get('welan', [WelanController::class, 'index']);
        Route::get('welan-add', [WelanController::class, 'create']);
        Route::post('welan-add', [WelanController::class, 'store']);
        
        Route::get('welan-edit/{slug}', [WelanController::class, 'edit']);
        Route::put('welan-edit/{slug}', [WelanController::class, 'update']);
        Route::get('welan-delete/{slug}', [WelanController::class, 'delete']);
        Route::get('welan-destroy/{slug}', [WelanController::class, 'destroy']);
        Route::get('welan-deleted', [WelanController::class, 'deletedwelan']);
        Route::get('welan-restore/{slug}', [WelanController::class, 'restore']);


        Route::get('framework', [FrameworkController::class, 'index']);
        Route::get('framework-add', [FrameworkController::class, 'create']);
        Route::post('framework-add', [FrameworkController::class, 'store']);
        Route::get('framework-edit/{slug}', [FrameworkController::class, 'edit']);
        Route::put('framework-edit/{slug}', [FrameworkController::class, 'update']);
        Route::get('framework-delete/{slug}', [FrameworkController::class, 'destroy']);
    
    });


});